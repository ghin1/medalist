import requests
import json
import pprint

ghinNum = "12345678"  # Your GHIN Number
password = "PASSWORD"  # Your password

headers = {
    'Content-Type': 'application/json; charset=utf-8',
    'Accept': 'application/json'
}

data = {
    'user': {
        'email_or_ghin': ghinNum,
        'password': password,
        'remember_me': 'true'
    },
    'token': 'nonblank'
}

data_json = json.dumps(data)

# Authenticating to GHIN
url = 'https://api2.ghin.com/api/v1/golfer_login.json'
auth_response = requests.post(url, data=data_json, headers=headers)
auth_data = auth_response.json()
token = auth_data['golfer_user']['golfer_user_token']
headers['Authorization'] = f'Bearer {token}'

# Golf lookup API endpoint

## Lookup by GHIN #
url = ("https://api.ghin.com/api/v1/golfers/search.json?per_page=16&page=1&golfer_id=%s&status=Active" % ghinNum)
response = requests.get(url, headers=headers)
data = response.json()
pprint.pprint(data['golfers'][0])

## Gives you this information
# # # first_name
# # # last_name
# # # gender
# # # email
# # # phone_number
# # # suffix
# # # prefix
# # # middle_name
# # # status
# # # ghin
# # # handicap_index
# # # association_id
# # # association_name
# # # club_name
# # # club_id
# # # state
# # # country
# # # low_hi
# # # soft_cap
# # # hard_cap
# # # entitlement
# # # club_affiliation_id
# # # is_home_club
# # # rev_date
# # # hi_value
# # # hi_display
# # # message_club_authorized
# # # low_hi_value
# # # low_hi_display
# # # low_hi_date
# # # city
# # # street_1
# # # street_2
# # # zip
# # # technology_provider
# # # local_number
# # # status_date
# # # membership_code
# # # hi_modified
# # # low_hi_modified
# # # low_hi_modified_date
# # # hi_withdrawn
# # # is_minor
# # # is_under_13
# # # player_name
# # # primary_club_country
# # # primary_club_state
# # # date_of_birth
# # # has_digital_profile
# # # is_under_19
# # # ojr_membership
# # # auto_renew_enabled
    
## Lookup by email
last_name = "LAST_NAME"
state = "OH" # abbreviated state
url = ("https://api.ghin.com/api/v1/golfers/search.json?per_page=16&page=1&last_name=%s&state=%s&status=Active" % (last_name, state))
response = requests.get(url, headers=headers)
data = response.json()
pprint.pprint(data['golfers'][0])

## Lookups available (does not include email) 
### golfer_id, last_name and state, last_name and country, last_name and association_id, or last_name and club_id with local_number
